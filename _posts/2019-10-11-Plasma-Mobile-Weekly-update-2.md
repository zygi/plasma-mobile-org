---
title:      "Plasma Mobile: weekly update: part 2"
created_at: 2019-10-11 20:07:00 UTC+5.30
author:     Plasma Mobile team
layout:     post
---

Thanks to the awesome Plasma Mobile community, we are happy to present a second weekly update from Plasma Mobile project.

## Shell user interface

Marco Martin made several changes in the shell to improve the overall user experience.

The application grid was updated to show application names in single line and with a smaller font size.

![Plasma Mobile app drawer](/img/screenshots/plamo-app-drawer.png){: .blog-post-image-small}

Marco Martin is also working on re-designing the top panel and top drawer, and bugfixes related to that. Below is screenshots of current state:

![Plasma Mobile top drawer](/img/screenshots/plamo-top-drawer.png){: .blog-post-image-small}

![Plasma Mobile notifications](/img/screenshots/plamo-notifcations.png){: .blog-post-image-small}

Both the top and bottom panels were updated to use the normal color scheme instead of the inverted/dark color scheme.

![Plasma Mobile home screen](/img/screenshots/plamo-home-screen.png){: .blog-post-image-small}

Marco martin also added several fixes [1](https://commits.kde.org/kwin/10ace126be219c0665e404e8dcf83a04c41035a3) and [2](https://commits.kde.org/kwin/1a9a49ab7a44ca354cd79409c90878b26706a042) in KWin/wayland for fullscreen windows used by the top drawer and the window switcher.


## Kirigami

Nicolas Fella [added new API to Kirigami](https://phabricator.kde.org/D24469) that allows us to make menus in a more traditional style on the desktop.

```
    globalDrawer: Kirigami.GlobalDrawer {
        isMenu: true
        actions: [
            Kirigami.Action {
                icon.name: "document-import"
                text: i18n("Import contacts")
                onTriggered: {
                    importFileDialog.open()
                }
            }
        ]
    }
```

Setting `isMenu` property to `true` on the `Drawer`, should hide the drawer handle when used on the desktop. Instead, a similar looking hamburger button should appear in the toolbar,
which behaves appropriately for the desktop.

## Applications

[Simon Schmeißer](https://invent.kde.org/schmeisser) added various improvements to the [QR-Code scanner application, qrca](https://invent.kde.org/jbbgameich/qrca/). It now suppports decoding vcard QR-Codes which include trailing spaces, and features a [Kirigami AboutPage](https://invent.kde.org/jbbgameich/qrca/merge_requests/5). The sheet that appears once a code has been decoded now doesn't flicker if the code is scanned a few times in a row. Jonah Brüchert ported the app's pageStack to [make use of the new Kirigami PagePool](https://invent.kde.org/jbbgameich/qrca/commit/72a599eafeadb14e1a547bac128e9e9b7c38ba52) introduced in last weeks blog post, which fixes page stack navigation issues with the About page.

Jonah Brüchert implemented [setting a photo for contacts in plasma-phonebook](https://invent.kde.org/kde/plasma-phonebook/commit/6e9f09ef38d7d867ed4f42da3fc8e470562448d8). Nicolas Fella improved the contacts list in the plasma-phonebook [simplifying codebase](https://invent.kde.org/kde/plasma-phonebook/commit/fd02f351240b5a2c0602b6fc488f7e2f1eb9b7b8). He also [reworked the code for individual contact actions](https://invent.kde.org/kde/plasma-phonebook/merge_requests/19) to make them functional and improve the scrolling experience.

Settings applications by default only shows the KCM modules which are suitable for mobile platform, Jonah Brüchert fixed the audio configuration KCM module to add the supported form factors key in desktop file, which makes Audio module visible in the Settings application again. If you are [developing a system settings module with Plasma Mobile](https://docs.plasma-mobile.org/PlasmaSettings.html) in mind, don't forget to add `X-KDE-FormFactors` key in the metadata.desktop file, e.g.

```
X-KDE-FormFactors=handset,tablet,desktop
```
[MauiKit](https://invent.kde.org/kde/mauikit) file management component now can make use of all the KIO supported protocols, like kdeconnect, applications, recentdocuments, fonts, etc, to browse your file system. This will allow you to seemlessly copy files and folders between various protcols like webdav and sftp. MauiKit has gained a couple of new components, designed to be used as visual delegates for list and grid views, one of those is the new `SwipeItemDelegate`, which works both on wide and small screen form factors. This delegate can contain quick action buttons which depending on the available space get shown inline when hovering, or underneath, revealing by a swipe gesture.

[Index, the file manager](https://invent.kde.org/kde/index-fm), has seen some feature improvements in the selection bar, when selected items are clicked you get a visual preview of the file and on long press the item is removed from the selection bar, making it easy to keep track of what you have selected. You can also mark files as Favorites and browse them easily in a new dedicated Quick section in the sidebar. The Miller column view now auto scrolls to the last column. By making use of the new MauiKit delegate controls, the file and directories on Index, can be dragged on top of each other to perfom actions like copy, move and link and also be dragged out of the app to be open or shared with an external application. Due to usage of KIO framework, Index can now browse various kio slaves like, applications, favorites, webdav, remote, recently used etc.\

---

![Index applications kioslave](/img/screenshots/index-apps.png){: .blog-post-image}
![Index favorites kioslave](/img/screenshots/index-fav.png){: .blog-post-image}
![Recent documents](/img/screenshots/index-recent-docs.png){: .blog-post-image}
![Remote slave](/img/screenshots/index-remote.png){: .blog-post-image}
![Webdav browsing Nextcloud](/img/screenshots/index-webdav.png){: .blog-post-image}

---

[vvave, the music player](https://invent.kde.org/kde/vvave), now has an improved Albums and Artist grid view, and a has gained a lot of small paper cut fixes to be ready for a release soon. If you are interested in helping testing this early packages and report back issues you can [join the telegram channel](https://t.me/mauiproject).

---

![vvave grid view](/img/screenshots/vvave-grid-view.png){: .blog-post-image}

![vvave swipeitem](/img/screenshots/vvave-swipeitem-delegate.png){: .blog-post-image}

---

## Downstream

Bhushan Shah worked on [several changes in postmarketOS](https://gitlab.com/postmarketOS/pmaports/merge_requests/672) to make telephony on devices like Pinephone and Librem 5 possible with Plasma Mobile. The [upstream change](https://invent.kde.org/kde/plasma-phone-components/merge_requests/27/) was suggested by [Alexander Akulich](https://invent.kde.org/akulichalexandr) to not hardcode a telepathy account name in the dialer source code.

We have successfully tested this change on Librem 5 developer kit.

![PlaMo making call](/img/plamo-calling-librem5.jpg){: .blog-post-image-center}

## Want to help?

Next time your name could be here! To find out the right task for you, from promotion to core system development, check out [Find your way in Plasma Mobile](/findyourway). We are also always happy to welcome new contributors on our [public channels](/join). See you there!
