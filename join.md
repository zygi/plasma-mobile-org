---
title: Join Plasma Mobile
permalink: /join/
layout: default
---

![Konqi](/img/424px-Mascot_konqi-app-internet.png){:width="200"}

If you'd like to contribute to the amazing free software for mobile devices, [join us - we always have a task for you](/contributing/)!

Plasma Mobile community groups and channels:

### Plasma Mobile specific channels:

* [![](/img/telegram.svg){:width="30"} Telegram](https://t.me/plasmamobile)

* [![](/img/matrix.svg){:width="30"} Matrix](https://matrix.to/#/#plasmamobile:matrix.org)

* [![](/img/irc.png){:width="30"} IRC](https://kiwiirc.com/nextclient/chat.freenode.net/#kde-plasmamobile)

* [![](/img/mail.svg){:width="30"} Plasma Mobile mailing list](https://mail.kde.org/mailman/listinfo/plasma-mobile)


### Plasma Mobile related project channels:

* [![](/img/irc.png){:width="30"} #plasma on Freenode (IRC)](https://kiwiirc.com/nextclient/chat.freenode.net/#plasma)

* [![](/img/telegram.svg){:width="30"} Halium on Telegram](https://t.me/Halium)

* [![](/img/mail.svg){:width="30"} Plasma development mailing list](https://mail.kde.org/mailman/listinfo/plasma-devel)
